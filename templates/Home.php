<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Bonjour</h1>

    <?php foreach ($articles as $article) { ?>
        <article>
        <h2><?=$article["Titre"] ?></h2>
        <h4><?=$article["Auteur"] ?> - <?=$article["Date"] ?> </h4>
        <p><?=$article["Contenu"] ?></p>
        <img src="<?=$article["image"] ?>" alt="<?=$article["Titre"] ?>"><br>
        <a href="?page=article&id=<?=$article["ID_article"] ?>">Lire plus</a>
    </article>
    <?php } ?>   
</body>
</html>