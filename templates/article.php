<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <header>
    <?php include "menu.php" ?>
    </header>
    <article>
            <h1><?=$article["Titre"] ?></h1>
            <h4><?=$article["Auteur"] ?> - <?=$article["Date"] ?> </h4>
            <img src="<?=$article["Image"] ?>" alt="<?=$article["Titre"] ?>">
            <p><?=$article["Contenu"] ?></p>   
        </article>
</body>
</html>