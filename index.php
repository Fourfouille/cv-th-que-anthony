<?php

require_once "vendor/autoload.php";
require_once "config/env.php";


if (isset($_GET["page"]) && $_GET["page"]) {
    
    //ici je mets en majuscule la premiere lettre de la valeur du paramètre "page"
    $page = ucfirst($_GET["page"]);
    //var_dump($page);

    $className = "App\\Controller\\{$page}Controller";
    //var_dump($className);

    $page =  new $className();
    //var_dump($page);


    // Ceci est une condition ternaire 
    //Si id est préent dans l'url et qu'il n'est pas vide, alors $id prend la valeur de l'url, sinon c'est null 
    $id = (isset($_GET["id"]) && $_GET["id"]) ? $_GET["id"] : null;

    if (isset($_GET["action"]) && $_GET["action"]) {
        // ici je recupere l'action dans l'url pour afficher la page concernée
        $method = $_GET["action"]."Page";
        $page->$method($id);
    } else {
        // s'il n'y a pas d'action, j'affiche la page normale
        $page->afficherPage($id);
    }

} else {
    // Par défaut j'affiche la page d'accueil
    $home = new App\Controller\HomeController();
    $home->afficherPage();
}

?>