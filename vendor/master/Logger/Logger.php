<?php
namespace Master\Logger;

class Logger
{
    public function logError($dateFormat, \PDOException $e, $messageType = 0, $destination = "")
    {
        $now = date($dateFormat);
        error_log(
            "{$now} : {$e->getFile()} - {$e->getLine()}\n Message : {$e->getMessage()}\n",
            $messageType,
            $destination
        );
    }
}