<?php

namespace App\Controller;

use App\Controller\ArticleController;
use App\Repository\ArticleRepository;

class HomeController
{
    public function afficherPage()
    {
        $aRepo = new ArticleRepository();
        $articles = $aRepo->getAllArticle();

        include "templates/home.php";
    }
}