<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;

class ArticleController
{
    public function createArticle($titre, $auteur,$date,$contenu,$image)
    {
        $article = new Article($titre, $auteur,$date,$contenu,$image);

        return $article;
    }


    public function afficherPage($id)
    {
        $aRepo = new ArticleRepository();
        $article = $aRepo->getOneArticle($id);
        include "templates/article.php";
    }

    public function editerPage()
    {
        echo "Page pour editer";
    }
}