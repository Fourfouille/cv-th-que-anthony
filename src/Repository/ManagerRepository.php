<?php

namespace App\Repository;

use PDO;

class ManagerRepository
{

    

    public function dbConnexion()
    {
        $connexion = new PDO(
            "mysql:host=localhost;dbname=alpha_blog;port=3306; charset=utf8",
            "root",
            "root"
        );

        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        return $connexion;
    }

    public function dbDeconnexion($connexion)
    {
        $connexion = null;
    }

}