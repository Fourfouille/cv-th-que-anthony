<?php

namespace App\Repository;

use Master\Logger\Logger;
use PDO;
use PDOException;


class ArticleRepository extends ManagerRepository
{
    
    public function getAllArticle()
    {
        try {
            //j'ai supprimé la function qui me permettait de ma connecter à la bdd
            // à la place j'en ai fait une fonction que j'appelle comme ceci 
            $connexion = $this->dbConnexion();

            $sql = "SELECT * FROM article";

            $stmt = $connexion->prepare($sql);
            $stmt->execute();
            // je recupère un tableau associatif
            $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // permet de fermer le connexion sql
            $this->dbDeconnexion($connexion);
        } catch (PDOException $e) {
            $log = new Logger();
            $log->logError(DATE_RFC822, $e, 3, ERROR_LOG_FILE);
        }
        // Lorsque je fais une requête en GET, il faut forcément que je fasse un return des données
        return $datas;
    }

    public function getOneArticle($id)
    {
        try {

            //j'ai supprimé la function qui me permettait de ma connecter à la bdd
            // à la place j'en ai fait une fonction que j'appelle comme ceci 
            $connexion = $this->dbConnexion();

                //j'aurai pu faire WHERE id_article={$id} mais ce n'est pas bien en termes de sécurité
            $sql = "SELECT * FROM article WHERE ID_article=:id";

            $stmt = $connexion->prepare($sql);
            // protection contre les injections sql
            $stmt->bindParam(":id", $id);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->dbDeconnexion($connexion);

        } catch(PDOException $e) {
            $log = new Logger();
            $log->logError(DATE_RFC822, $e, 3, ERROR_LOG_FILE);
        }
        return $data;
    }
}